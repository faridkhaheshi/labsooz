# labsooz

This service will provide small laboratories with a basic tool to send their test results to their patients.

## Standard for Error and Status Codes ##

All endpoints respond based on this guideline.

### Response body when successful ###

If the user is asking for some data, the response body will include only the data demanded. For example, if the user is sending a `GET` request to `/api/results/:resultId` the response body will include only the result with the given id.

```
{
    "_id": "1234",
    "photoUrl": "https://somecloud.com/filename.jpg",
    "clientMobileNumber": "09123456789",
    "lab": "labid",
    "createdAt": "2019-08-05T11:35:41.737Z",
    "updatedAt": "2019-08-05T11:35:41.737Z",
    "__v": 0
}
```

There will be no data property to include the actual data. There will be no extra message in the response body.

But if the request does not demand specific data but asks for a job to be done, the response body will include the following information:

```
{
    "done": true,
    "trackId": "abcdef",
    "message": "the one-time password was successfully sent to mobile number +989123456789",
    "data": {
        "expiresInSeconds": 600
    }
}
```

The `track-id` can be used for further checks. The `data` field will include any data that may be needed by the user.

### Status Code when successful ###

If the request is successful, the status code will be one of the followings:
* `200 (OK)`: it means that the request was served successfully. This is the general status code that is sent in response to `GET`, `PUT`, or `DELETE` requests in response to which no new object or entity is created. 
* `201 (CREATED)`: this status code is sent when an object or entity is created and added to the system. For example, `POST` request to `/api/results` will add new results to the system.
* `202 (ACCEPTED)`: For now, we don't use this status code for any request. But we may use it to respond to requests that start a process, but no specific change will be made immediately because more steps must be taken.

### Response body when unsuccessful ###

When the request is not successful, the response body will include an `error` property with the following information:

```
{
   "error":{
       "code": 102,
       "type": "AUTHORIZATION_ERROR",
       "message": "Wrong Credentials",
       "moreInfo": "You have provided the wrong token",
       "stackTrace": ""
   }
}
```
The `stackTrace` will not be sent to external users and used only for internal process. The `moreInfo` property may not be available for all errors. But ALL errors will include the other three propertires.

#### List of Error Types ####

All possible error types are listed in the following table:

| Error Type | Description |
| :----------: | :----------- |
| ValidationError | The parameters sent in the request are not in the expected form. |
| PermissionError | The user is not permitted to request. |
| DatabaseError   | There was some problem in database. |
| WrongInputError      | Wrong input is provided.|
| AccountLimitError | The request is not available for the current account type |
| InternalError | General error for internal problems (usually unknown) |

#### List of Error Codes ####

All possible error codes are shown in the following table:

| Error Code | Description |
| :----------: | :----------- |
| 100 | This error happens when the user tries to access unprotected routes without providing a valid mobile number. |
| 101 | To access this route, you should provide a valid mobile number. |
| 102 | This happens when the user provides no otp to the login api. |
| 103 | This error happens when the user provides an invalid one-time password. |
| 104 | Something went wrong when trying to check you one-time password. We are trying to solve the problem. |
| 105 | This happens when a database operation fails for some reasons. | 
| 106 | This happens when the user provides no authorization header with her request. | 
| 107 | This happens when the user has provided an authorization header, but the parameter is not in valid format. | 
| 108 | This happens when the verification of the jwt token fails. It is probably related to providing a wrong token or an invalid or expired one. | 
| 109 | This happens when the input to the request is not provided in valid format. | 
| 110 | This happens when the user has provided a valid jwt token, but has not the required privilege to access the route. | 
| 111 | This happens when the user is trying to get access to a lab route with a labId that is not valid. No lab can be found with this id. |
| 112 | This happens when the user is trying to update her own lab but gets validation because some parameters in req.body are not given in the right format. | 
| 113 | This happens when a lab owner tries to add a user to her lab but the maximum user limit for the lab is reached. |
| 114 | This happens when a lab user is adding a new result but a problem occurs in the messenger service and the sms is not sent to the user. | 
| 115 | This happens when a request is received to get results of a particular lab but the query is not valid. | 
| 116 | This happens when a lab user is trying to get an upload permission but for some reason the underlying services does not work. | 
| 117 | This happens when a lab user is trying to edit a result but the input she provides is not valid. | 
| 118 | This error happens when the lab user is trying to edit a result but the result id he provides can not be found in db. |
| 404 | This is the general not found response. It means that the user is looking for something that does not exists. | 
| 999 | We don't know the problem yet. We should find the cause and add appropriate error message. | 

### Status Code when unsuccessful ###

If the request is not successfully handled as expected, the status code will be either in the `4XX` range or in the `5XX` range. 

The `4XX` range indicates that there was a problem in the request sent by the user. In this case, to get a successful response, the user should change the request and submit again. 

The `5XX` range indicates that there was an internal problem when trying to serve the user. The user may try again later with the same parameters and get the expected response. We will use ONLY the following status codes:

Specific status codes in the `4XX` range are:

* `400 (BAD REQUEST)`: it is the general status code used for something that is perceived to be  a client error. We use it only for cases to which other status code in the `4XX` range cannot be applied.
* `401 (UNAUTHORIZED)`: this is used when there is a problem with user credentials. It may be due to wrong authorization token or no token at all. This means that if the user provides the right authenticatin credentials, she may be served successfullu.
* `403 (FORBIDDEN)`: this is used when the user has provided the credentials but the credentials provided do not give her access to the resource.
* `404 (NOT FOUND)`: this means that the entity requested by the user cannot be found. For example, if the user asks for a `result` with specifid id (`resultId`) by sending a `GET` request to `/api/results/:resultId` and there is no result with this id in database.
* `409 (CONFLICT)`: For now, we do not return this status code. But we may add it later to indicate conflict. A conflict may happen for example when two different users are sending `PUT` requests to change the same `result`.
* `429 (TOO MANY REQUESTS)`: this is sent when a lot of requests are received from the same user.

Specific status codes in the `5XX` range are:
* `500 (INTERNAL SERVER ERROR)`: this is the general status code sent for internal errors. More information about the error will be available in the response body.
* `503 (Service Unavailable)`: this code will be used when the service is (hopefully) temporarily unavailable. The user can hope that the service will be available soon and she can send her request again.

> At the moment, we use only the following status codes: `200`, `201`, `400`, `401`, `403`, `404`, `500`, `503`

## All api end-points ##

Here, we describe the behavior of all api end-points.

### `POST` /auth/otp###

This is responsible for giving a one-time password to a valid user.

* **success**: a success response will have a `200` status code. The response body will include the following fields:

```
{
    "done": true,
    "trackId": "",
    "message": "The one-time password was successfully sent to +989123456789",
    "data": {
        "expiresInSeconds": 600
    }
}
```

* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 100 | An invalid `mobileNumber` is provided as input | 400 |
| 101 | There was some problem in sending the one-time password to the `mobileNumber` provided | 500 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |

In any case, the response will include the following data about the error: 

```
{
    "error": {
        "code": <error code as a number>,
        "type": "error type as string",
        "message": "error message"
        "moreInfo": "more info"
    }   
}
```


### `POST` /auth/login ###

This is responsible for generating a valid access token for the user. It checks the given one-time password and if it is valid, sends back a jwt token.

* **success**: a success response will have a `200` status code. The response body will include the following fields:

```
{
    "token": "jwt token as string",
    "primaryLab": {
        "_id": "lab id as string",
        "name": "lab name as string",
        "logoUrl": "http://www.labwebsite.ir/logofile.png",
        "description": "lab description as string"
    }
}
```

* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 100 | An invalid `mobileNumber` is provided as input | 400 |
| 102 | No `otp` in provided in the request body | 400 |
| 103 | The `otp` provided is invalid | 401 |
| 104 | There was a problem in the internal services used for login | 500 |
| 105 | There was a database problem | 500 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |

In any case, the response will include the following data about the error: 

```
{
    "error": {
        "code": <error code as a number>,
        "type": "error type as string",
        "message": "error message"
        "moreInfo": "more info"
    }   
}
```

### `GET` /api/labs ###

This api is responsible for giving all labs owned by the user. The user is authenticated and identified by the
token given in the request header. The result can be filtered by providing query as search parameters
in the url.

* **success**: a success response will have a status `200` status code. The response body will include all labs owned by the user in an array:

```
[
    {
        "users": [
            "5d8ee9dab5cdd90004039fa5",
            "5d905a4b4ce4450004cefc6d"
        ],
        "_id": "5d8705be9d47d40004bef873",
        "name": "آزمایشگاه سلامتی",
        "owner": "5d8704a49d47d40004bef872",
        "createdBy": "5d8704a49d47d40004bef872",
        "createdAt": "2019-09-22T05:25:18.171Z",
        "updatedAt": "2019-09-29T08:56:54.811Z",
        "__v": 0,
        "description": "دم شما گرم "
    }
]
```

* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 109 | The inputs provided through query are wrong and caused validation error | 400 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |


In any case, the response will include the following data about the error: 

```
{
    "error": {
        "code": <error code as a number>,
        "type": "error type as string",
        "message": "error message",
        "moreInfo": "more info"
    }   
}
```

### `POST` /api/labs ###

This api ir responsible for creating a new lab for the user. The user is identified by providing a valid token in the request header.

Request body should provide the lab data. 

```
{
  "name": "some name for the lab",
  "logoUrl": "https://the.lab.com/logo.jpeg",
  "description": "some description for the lab"
}
```

The `name` field is required.

If the data is provided correctly, and the user is authorized to create a new lab, the new lab is created and returned in response.

* **success**: a success response will have a status `200` status code. The response body will include the new lab created:

```
{
    "users": [],
    "_id": "5d91eff2ababa60004af2647",
    "name": "test lab 1020",
    "owner": "5d8704a49d47d40004bef872",
    "createdBy": "5d8704a49d47d40004bef872",
    "createdAt": "2019-09-30T12:07:14.883Z",
    "updatedAt": "2019-09-30T12:07:14.883Z",
    "__v": 0
}
```

* **error**: the following table shows all possible error cases along with the status code and error code:

| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 109 | The inputs provided in request body are wrong. You should provide the name and other fields for the lab | 400 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |


In any case, the response will include the following data about the error: 

```
{
  "error": {
    "code": <error code as a number>,
    "type": "error type as string",
    "message": "error message",
    "moreInfo": "more info"
  }
}
```


### `POST` /api/labs/:labId/users ###

This api is responsible for adding users to the lab. It can only be used by the lab owner. The lab owner is authenticated and authorized by the token provided in request header.

Request body should provide the user data. The `mobileNumber` field is mandatory for adding a
 user to the lab. The request body may be something like this:


```
{
  "mobileNumber": "09381269741"
}
```

If the user data provided in request body is correct, and the lab has not reached its maximum number of users, the new user is added to the lab. The response will provided the updated lab info in the `data` field.

* **success**: a success response will have a status `200` status code. The response body will include
the following info:

```
{
  "done": true,
  "message": "The user successfully added to the lab",
  "data": {
    "users": [
      "5d92e9981ef74e0004c9b2cc",
      "5d92e9bc1ef74e0004c9b2cd"
    ],
    "_id": "5d91edcaababa60004af2645",
    "name": "آزمایشگاه سلامتی",
    "owner": "5d8704a49d47d40004bef872",
    "createdBy": "5d8704a49d47d40004bef872",
    "createdAt": "2019-09-30T11:58:02.326Z",
    "updatedAt": "2019-10-01T05:53:00.468Z",
    "__v": 0,
    "description": "ما خیلی خوبیم"
  }
}
```


* **error**: the following table shows all possible error cases along with the status code and error code:

| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 109 | The input in the request body is not provided in a valid format | 400 |
| 110 | The token provided for authorization is valid, but is not eligible for adding user to this lab | 403 |
| 111 | The id provided for the lab in the url is wrong | 404 |
| 113 | The maximum number of users for this lab is reached | 402 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |


In any case, the response will include the following data about the error: 

```
{
  "error": {
    "code": <error code as a number>,
    "type": "error type as string",
    "message": "error message",
    "moreInfo": "more info"
  }
}
```


### `GET` /api/labs/:labId/users/:userId ###

This api gives the data about the user specified by the userId. The user must be a user of the lab specified by labId. The user is authenticated by checking the token provided in request header. The user must be the owner of the the lab. 

* **success**: a success response will have a status `200` status code. The response body will include the user data:

```
{
  "isVerified": false,
  "_id": "5d92f15c1ef74e0004c9b2d1",
  "mobileNumber": "+989123456789",
  "firstName": "فرهاد",
  "lastName": "غضنفری",
  "createdAt": "2019-10-01T06:25:32.616Z",
  "updatedAt": "2019-10-01T06:25:32.624Z",
  "__v": 0
}
```

* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 110 | The provided token is valid but is not allowed to access this route | 403 |
| 111 | The id provided for the lab in the url is wrong | 404 |
| 404 | The user was not found. The id must be wrong. | 404 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |


In any case, the response will include the following data about the error: 

```
{
  "error": {
    "code": <error code as a number>,
    "type": "error type as string",
    "message": "error message",
    "moreInfo": "more info"
  }
}
```


### `DEL` /api/labs/:labId/users/:userId ###

This api removes the user specified by `userId` in the url from the lab specified by the `labId`.
The user specified by `userId` must already be a user of the lab specified by the `labId` and the user requesting this action must be the owner of the lab.


* **success**: a success response will have a status `200` status code. The response body will include the new lab in the `data` field:



```
{
  "done": true,
  "message": "The user 5d92e9981ef74e0004c9b2cc is no longer a user of your lab.",
  "data": {
    "users": [
      "5d92e9bc1ef74e0004c9b2cd",
      "5d92ec641ef74e0004c9b2d0",
      "5d92f15c1ef74e0004c9b2d1"
    ],
    "_id": "5d91edcaababa60004af2645",
    "name": "آزمایشگاه سلامتی",
    "owner": "5d8704a49d47d40004bef872",
    "createdBy": "5d8704a49d47d40004bef872",
    "createdAt": "2019-09-30T11:58:02.326Z",
    "updatedAt": "2019-10-01T07:04:02.787Z",
    "__v": 0,
    "description": "ما خیلی خوبیم"
  }
}
```



* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 109 | The input to the api is not provided in a valid format | 400 |
| 110 | The provided token is valid but is not allowed to access this route | 403 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |


In any case, the response will include the following data about the error: 

```
{
  "error": {
    "code": <error code as a number>,
    "type": "error type as string",
    "message": "error message",
    "moreInfo": "more info"
  }
}
```



### `GET` /api/labs/:labId/results ###

This api is responsible for giving lab results. The user requesting must be a user or 
the owner of the lab specified by `labId`. The service returns the last 20 results of 
the lab.
Older results can be obtained by providing a `createdBefore` field in query. The query
data must be provided using the search terms in the request url.

* **success**: a success response will have a status `200` status code. The response body will be an array of the results:



```
[
  {
    "_id": "5d92ff617bb763000483de59",
    "photoUrl": "http://res.cloudinary.com/nimvajabicloud/image/upload/v1569914716/yuyjhgkoc72hw7tutqsf.jpg",
    "clientMobileNumber": "+989351269741",
    "createdBy": {
        "_id": "5d8704a49d47d40004bef872",
        "mobileNumber": "+989351269741"
    },
    "lab": "5d91edcaababa60004af2645",
    "createdAt": "2019-10-01T07:25:21.658Z",
    "updatedAt": "2019-10-01T07:25:21.658Z",
    "__v": 0
  }
]
```

* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 110 | The provided token is valid but is not allowed to access this route | 403 |
| 111 | The id provided for the lab in the url is wrong | 404 |
| 115 | The query is wrong| 400 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |


In any case, the response will include the following data about the error: 

```
{
  "error": {
    "code": <error code as a number>,
    "type": "error type as string",
    "message": "error message",
    "moreInfo": "more info"
  }
}
```

### `PUT` /api/labs/:labId ###

This service is responsible for changing the lab info. The user should be the owner of
the lab or an admin. The user is authenticated based on the token provided in 
request header.

* **success**: a success response will have a status `200` status code. The response body will include the new lab data:


```
{
  "users": [
    "5d92ec641ef74e0004c9b2d0",
    "5d92f15c1ef74e0004c9b2d1"
  ],
  "_id": "5d91edcaababa60004af2645",
  "name": "آزمایشگاه سلامتی",
  "owner": "5d8704a49d47d40004bef872",
  "createdBy": "5d8704a49d47d40004bef872",
  "createdAt": "2019-09-30T11:58:02.326Z",
  "updatedAt": "2019-10-01T08:09:42.501Z",
  "__v": 0,
  "description": "ما شما را آزمایش می‌کنیم!"
}
```


* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 110 | The provided token is valid but is not allowed to access this route | 403 |
| 111 | The id provided for the lab in the url is wrong | 404 |
| 112 | Parameters in request body are wrong| 400 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |


In any case, the response will include the following data about the error: 

```
{
  "error": {
    "code": <error code as a number>,
    "type": "error type as string",
    "message": "error message",
    "moreInfo": "more info"
  }
}
```



### `GET` /api/labs/:labId/upload ###

This endpoint is responsible for issuing an upload permission for the client.

* **success**: a success response will have a status `200` status code. The response body will include the following fields:

```
{
  "uploadUrl": "jwt token as string",
  "formDataInfo": {
    "timestamp": "e.g. 1565434011 as string",
    "api_key": "e.g. 312616161616 as string",
    "signature": "some signature as string",
  }
}
```

The `formDataInfo` should be used by the client to create formData. Each property of `formDataInfo` should be mapped to a key in the resulting formData. Other properties may be added to this field. 

* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 110 | The token provided in `Authorization` header cannot access this route | 403 |
| 111 | The labId provided in the route cannot be found | 404 |
| 116 | For some internal problems of the systems, an upload permission cannot be issued at the moment | 500 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |

In any case, the response will include the following data about the error: 

```
{
    "error": {
        "code": <error code as a number>,
        "type": "error type as string",
        "message": "error message"
        "moreInfo": "more info"
    }   
}
```


### `POST` /api/labs/:labId/results ###

This endpoint is responsible for adding results for the given lab (`labId`).

* **success**: a success response will have a `200` status code. The response body will include the following fields:

```
{
    "done": true,
    "message": "e.g.: new result added anc client informed",
    "data": {
        "_id": "result id as string",
        "photoUrl": "photo url as string",
        "clientMobileNumber": "e.g.: +989121234567 as string",
        "createdBy": "user id as string",
        "lab": "lab id as string",
        "createdAt": "2019-08-12T12:02:27.016Z",
        "updatedAt": "createdAt": "2019-08-12T12:02:27.016Z",
        "__v": 0
    }
}
```

* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 106 | No `Authorization` header is provided | 401 |
| 107 | The `Authorization` header is provided but it is not in valid format | 401 |
| 108 | The `Authorization` header is invalid | 401 |
| 109 | Wrong input is provided to the api. This is the basic validation error. | 400 |
| 110 | The token provided in `Authorization` header cannot access this route | 403 |
| 111 | The labId provided in the route cannot be found | 404 |
| 114 | The result is created but for some internal problems the sms is not sent to the user  | 500 |
| 116 | For some internal problems of the systems, an upload permission cannot be issued at the moment | 500 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |

In any case, the response will include the following data about the error: 

```
{
    "error": {
        "code": <error code as a number>,
        "type": "error type as string",
        "message": "error message"
        "moreInfo": "more info"
    }   
}
```

### `GET` /api/results/:resultId ###

This service returns the result info for the result specified by the given `resultId`.


* **success**: a success response will have a `200` status code. The response body will include the result:


```
{
    "_id": "5d92ff617bb763000483de59",
    "photoUrl": "http://res.cloudinary.com/nimvajabicloud/image/upload/v1569914716/yuyjhgkoc72hw7tutqsf.jpg",
    "lab": {
        "_id": "5d91edcaababa60004af2645",
        "name": "آزمایشگاه سلامتی",
        "description": "ما شما را آزمایش می‌کنیم!"
    },
    "__v": 0
}
```



* **error**: the following table shows all possible error cases along with the status code and error code:


| Error Code | Description | Status Code |
| :----------: | :----------- | :-------: |
| 105 | There was a database problem | 500 |
| 109 | Wrong input is provided to the api. This is the basic validation error. | 400 |
| 404 | The result was not found. The id must be wrong. | 404 |
| 999 | Something went wrong in the server. The root cause of the error is unknown at the moment | 500 |

In any case, the response will include the following data about the error: 

```
{
    "error": {
        "code": <error code as a number>,
        "type": "error type as string",
        "message": "error message"
        "moreInfo": "more info"
    }   
}
```