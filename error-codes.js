const errorTypes = {
  ValidationError: "ValidationError",
  PermissionError: "PermissionError",
  DatabaseError: "DatabaseError",
  WrongInputError: "WrongInputError",
  ServiceError: "ServiceError",
  InternalError: "InternalError",
  AccountLimitError: "AccountLimitError"
};

const errorCodes = {
  100: {
    description:
      "This error happens when the user tries to access unprotected routes without providing a valid mobile number.",
    message: "To access this route you should provide a valid mobile number.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ValidationError
  },
  101: {
    description:
      "This happens when there is some error in the otpService.sendOtpTo method. It may be related to our sms service provider. Check console logs for more information.",
    message:
      "Problem when trying to send the otp to the provided mobile number. We are trying to solve the problem.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ServiceError
  },
  102: {
    description:
      "This happens when the user tries to login without providing a valid one-time password.",
    message: "You should provice a one-time password to login.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ValidationError
  },
  103: {
    description:
      "This error happens when the user provides an invalid one-time password.",
    message: "The one-time password is invalid.",
    moreInfo:
      "The password you provided is either wrong or used before. For more information, please refer to documentations.",
    type: errorTypes.PermissionError
  },
  104: {
    description:
      "This happens in login.auth.controller when there is a problem either in otp.service or token.service",
    message:
      "Something went wrong when trying to check you one-time password. We are trying to solve the problem.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ServiceError
  },
  105: {
    description:
      "This happens when a database operation fails for some reasons. It may occur at many places. We should check console logs to find the root cause.",
    message:
      "There was some database problem. sorry! We are trying to solve the problem.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.DatabaseError
  },
  106: {
    description:
      "This happens when the user provides no authorization header with her request.",
    message: "Authorization is mandatory.",
    moreInfo: "You should provide an Authorization header.",
    type: errorTypes.PermissionError
  },
  107: {
    description:
      "This happens when the user has provided an authorization header, but the parameter is not in valid format.",
    message: "The Authorization token is not in the right format.",
    moreInfo: "Please search for 'Bearer Authorization token'.",
    type: errorTypes.PermissionError
  },
  108: {
    description:
      "This happens when the verification of the jwt token fails. It is probably related to providing a wrong token or an invalid or expired one.",
    message: "The authorization token is invalid.",
    moreInfo:
      "It may be expired or deformed. Please search for 'jwt token' validation.",
    type: errorTypes.PermissionError
  },
  109: {
    description:
      "This happens when the input to the request is not provided in valid format.",
    message: "Wrong input.",
    moreInfo:
      "Refer to documentations to see the valid formats of the input parameters.",
    type: errorTypes.ValidationError
  },
  110: {
    description:
      "This happens when the user has provided a valid jwt token, but has not the required privilege to access the route.",
    message: "Forbidden.",
    moreInfo: "Access to this route is limited.",
    type: errorTypes.PermissionError
  },
  111: {
    description:
      "This happens when the user is trying to get access to a lab route with a labId that is not valid. No lab can be found with this id.",
    message: "Wrong id is provided for lab",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ValidationError
  },
  112: {
    description:
      "This happens when the user is trying to update her own lab but gets validation because some parameters in req.body are not given in the right format.",
    message: "Invalid input format",
    moreInfo:
      "To update the the lab information you should provide the update information in the right format.",
    type: errorTypes.ValidationError
  },
  113: {
    description:
      "This happens when a lab owner tries to add a user to her lab but the maximum user limit for the lab is reached.",
    message: "Maximum user limit for the lab reached.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.AccountLimitError
  },
  114: {
    description:
      "This happens when a lab user is adding a new result but a problem occurs in the messenger service and the sms is not sent to the user.",
    message:
      "Problem when trying to send the result to the provided mobile number. We are trying to solve the problem.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ServiceError
  },
  115: {
    description:
      "This happens when a request is received to get results of a particular lab but the query is not valid.",
    message: "Invalid input.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ValidationError
  },
  116: {
    description:
      "This happens when a lab user is trying to get an upload permission but for some reason the underlying services does not work.",
    message:
      "We are experiencing some problems with our image upload service providers. We are trying to solve it.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ServiceError
  },
  117: {
    description:
      "This happens when a lab user is trying to edit a result but the input she provides is not valid.",
    message: "Invalid input.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.ValidationError
  },
  118: {
    description:
      "This error happens when the lab user is trying to edit a result but the result id he provides can not be found in db.",
    message: "No result found with the given id.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.WrongInputError
  },
  119: {
    description:
      "This happens when the admin is trying to reactivate a lab but gets validation because some parameters in req.body are not given in the right format.",
    message: "Invalid input format",
    moreInfo:
      "To update the the lab information you should provide the update information in the right format.",
    type: errorTypes.ValidationError
  },
  120: {
    description:
      "This error happens when the user is not in the whitelist of the users who can use the service in the limited phase.",
    message: "Forbidden .",
    moreInfo: "The service is not yet available for public use.",
    type: errorTypes.PermissionError
  },
  121: {
    description:
      "This happens when the user tries to pay an invoice that has already been paid or does not exist.",
    message: "This invoice does not exists or has already been paid.",
    moreInfo: "Ensure you have provided the right invoice id.",
    type: errorTypes.WrongInputError
  },
  122: {
    description:
      "This happens when the lab is run out of credit and therefore cannot submit new results.",
    message: "No credit. Please charge your lab account.",
    moreInfo:
      "Inform the lab owner to charge lab account on lab.bistopanj.com.",
    type: errorTypes.AccountLimitError
  },
  404: {
    description:
      "This is the general not found response. It means that the user is looking for something that does not exists.",
    message: "Not Found.",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.WrongInputError
  },
  999: {
    description:
      "We don't know the problem yet. We should find the cause and add appropriate error message.",
    message: "There was an internal problem. Sorry!",
    moreInfo: "Please refer to api documentations.",
    type: errorTypes.InternalError
  }
};

module.exports = {
  errorTypes,
  errorCodes
};
