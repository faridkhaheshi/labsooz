module.exports = ({ Invoice, errorService, invoiceService }) => {
  const addInvoice = async (req, res) => {
    try {
      const invoiceData = req.body || {};
      const { _id: userId } = req.user;
      invoiceData.createdBy = userId;
      const newInvoice = await invoiceService.create(invoiceData);
      res.json(newInvoice);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const find = async (req, res) => {
    try {
      const labs = await invoiceService.find(req.query);
      res.json(labs);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const clearAll = async (req, res) => {
    try {
      await invoiceService.deleteMany({});
      res.json({
        done: true,
        message: "All invoices removed from database! Are you satisfied?"
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const cancelInvoice = async (req, res) => {
    try {
      const updatedInvoice = await invoiceService.update(req.params.invoiceId, {
        isValid: false
      });
      res.json(updatedInvoice);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const payInvoice = async (req, res) => {
    try {
      const result = await invoiceService.pay(req.params.invoiceId);
      res.json({
        done: true,
        message: "The invoice is paid successfully.",
        data: result
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          WrongInputError: { code: 121, custom: true, status: 400 },
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  return {
    add: addInvoice,
    find,
    clearAll,
    cancel: cancelInvoice,
    pay: payInvoice
  };
};
