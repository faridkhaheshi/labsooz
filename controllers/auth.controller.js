module.exports = ({
  otpService,
  tools,
  userService,
  labService,
  tokenService,
  errorService,
  appOwnerMobileNumber,
  params
}) => {
  /*
        This middleware checks to ensure that the user is a user of the lab.
        Passes the request to the next middleware is this check is passed.
        We have the user in req.body;
    */
  const restrictToLabUsers = async (req, res, next) => {
    try {
      const lab = req.lab;
      if (
        lab.users.includes(req.user._id) ||
        req.user._id == lab.owner.toString()
      ) {
        next();
      } else {
        res.status(403).send({ error: errorService.getDefaultError(110) });
      }
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error
      });
      res.status(status).json({ error: userError });
    }
  };

  /*
        Middleware to limit access to lab owner or admin
    */
  const restrictToLabOwnerOrAdmin = async (req, res, next) => {
    try {
      const lab = req.lab;
      const userId = req.user._id;
      const user = await userService.findById(userId);
      if (
        (user || {}).type === params.userTypes.admin ||
        (user || {}).mobileNumber === appOwnerMobileNumber ||
        (lab || {}).owner == userId
      ) {
        req.user = user;
        next();
      } else
        return res
          .status(403)
          .json({ error: errorService.getDefaultError(110) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 110, custom: false, status: 403 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const restrictToAdminOrSelf = async (req, res, next) => {
    try {
      const userId = req.user._id;
      console.log(`params.userId: ${req.params.userId}`);
      console.log(`userId: ${userId}`);
      const user = await userService.findById(userId);
      if (
        (user || {}).type === params.userTypes.admin ||
        (user || {}).mobileNumber === appOwnerMobileNumber ||
        req.params.userId == userId
      ) {
        req.user = user;
        next();
      } else
        return res
          .status(403)
          .json({ error: errorService.getDefaultError(110) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 110, custom: false, status: 403 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  /*
        This middleware checks to ensure that the user is the owner of the lab.
        Passes the request to the next middleware if check is passed.
        It also adds the lab object to req.lab.
    */
  const restrictToLabOwner = async (req, res, next) => {
    const userId = req.user._id;
    const lab = req.lab;
    if ((lab || {}).owner == userId) {
      req.lab = lab;
      next();
    } else {
      res.status(403).send({ error: errorService.getDefaultError(110) });
    }
  };

  /*
        This middleware restricts the access to a route to app owner.
        This is the highest level of restriction.

    */
  const restrictToAppOwner = async (req, res, next) => {
    try {
      const user = await userService.findById(req.user._id);
      if ((user || {}).mobileNumber === appOwnerMobileNumber) {
        req.user = user;
        next();
      } else
        return res
          .status(403)
          .json({ error: errorService.getDefaultError(110) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {}
      });
      res.status(status).json({ error: userError });
    }
  };

  /*
        This middleware checks to ensure that the user is an admin.
        Passes the request to the next middleware if the check is passed.
    */
  const restrictToAdmin = async (req, res, next) => {
    try {
      const user = await userService.findById(req.user._id);
      if (
        (user || {}).type === params.userTypes.admin ||
        (user || {}).mobileNumber === appOwnerMobileNumber
      ) {
        req.user = user;
        next();
      } else
        return res
          .status(403)
          .json({ error: errorService.getDefaultError(110) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 110, custom: false, status: 403 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const restrictToCandidates = async (req, res, next) => {
    try {
      const user = await userService.findById(req.user._id);
      const { mobileNumber } = user || {};
      const isWhitelisted = await userService.isCandidate({ mobileNumber });
      if (isWhitelisted) next();
      else
        return res
          .status(403)
          .json({ error: errorService.getDefaultError(120) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 110, custom: false, status: 403 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  /*
  White list includes:
    1. current users.
    2. candidates.
    3. app owner
  */
  const restrictToWhitelist = async (req, res, next) => {
    try {
      const { standardFormat: mobileNumber = null } = req.mobileNumber;
      const user = await userService.findOne({ mobileNumber });
      if (user) {
        req.user = user;
        next();
      } else {
        const isCandid = await userService.isCandidate({ mobileNumber });
        if (isCandid || userService.isAppOwner(mobileNumber)) next();
        else res.status(403).json({ error: errorService.getDefaultError(120) });
      }
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  /* 
        This is a middleware for the protected routes.
        We check the jwt token sent as a header and proceed to the next step only
        if a valid jwt is provided. 
        We also put the user object in req.user;
    */
  const validateToken = async (req, res, next) => {
    const authHeader = req.headers["authorization"];
    if (!authHeader)
      return res.status(401).json({ error: errorService.getDefaultError(106) });

    const authHeaderParts = authHeader.split(" ");
    if (authHeaderParts.length !== 2)
      return res.status(401).json({ error: errorService.getDefaultError(107) });
    const token = authHeaderParts[1];
    try {
      const payload = await tokenService.verify(token);
      req.user = payload.user;
      next();
      return;
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          PermissionError: { code: 108, custom: false, status: 401 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // handle verification of users to access unprotected routes.
  const validateUser = (req, res, next) => {
    const { mobileNumber = null } = req.body;
    const stdMobileNumber = tools.parseMobileNumber(mobileNumber);
    if (stdMobileNumber) {
      req.mobileNumber = stdMobileNumber;
      next();
    } else {
      res.status(400).json({ error: errorService.getDefaultError(100) });
    }
  };

  const askForOtp = async (req, res) => {
    try {
      const { standardFormat: mobileNumber = null } = req.mobileNumber;
      const { reviewers } = params;
      if (mobileNumber in reviewers)
        return res.json({
          done: true,
          trackId: "",
          message: "You must have the password!",
          data: {}
        });
      const data = await otpService.sendOtpTo(mobileNumber);
      res.json({
        done: true,
        trackId: "",
        message: `The One-time Password was successfully sent to ${mobileNumber}`,
        data
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ServiceError: { code: 101, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const login = async (req, res) => {
    const { otp } = req.body;
    const mobileNumber = req.mobileNumber.standardFormat;
    if (!otp)
      return res.status(400).json({ error: errorService.getDefaultError(102) });

    try {
      const isValid = await otpService.checkOtp({ otp, mobileNumber });
      if (isValid) {
        /*
                    1. create or update user. => user id
                    2. generate token.
                    3. send token to user.
                */
        const user = await userService.UpsertUser(
          { mobileNumber },
          { isVerified: true },
          "primaryLab"
        ); // 1.
        const token = await tokenService.generateFor(user); // 2.
        const data = { token };
        if (user.primaryLab) {
          data.primaryLab = {
            _id: user.primaryLab._id,
            name: user.primaryLab.name || "",
            logoUrl: user.primaryLab.logoUrl || "",
            description: user.primaryLab.description || ""
          };
        }

        res.json(data); // 3.
      } else res.status(401).json({ error: errorService.getDefaultError(103) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ServiceError: { code: 104, custom: false, status: 500 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  return {
    askForOtp,
    login,
    validateUser,
    validateToken,
    restrictToAdmin,
    restrictToLabOwner,
    restrictToLabUsers,
    restrictToAppOwner,
    restrictToLabOwnerOrAdmin,
    restrictToAdminOrSelf,
    restrictToCandidates,
    restrictToWhitelist
  };
};
