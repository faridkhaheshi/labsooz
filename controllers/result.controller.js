module.exports = ({
  Result,
  interfaceMessages,
  messenger,
  mustache,
  tools,
  resultService,
  errorService,
  params
}) => {
  /*
		This method creates a new Result using the data provided through the req.body and saves it in db.
		We have the user in req.user.
		We have the lab in req.lab.
		We are sure that the user is a lab user and can add the result for the lab.

	*/

  const addResult = async (req, res) => {
    try {
      resultService.normalizeResultData(req.body);
      req.body.createdBy = req.user._id.toString();
      req.body.lab = req.lab._id.toString();
      const savedResult = await resultService.add({
        result: req.body,
        lab: req.lab
      });

      if (!req.body.informClient)
        return res.json({
          done: true,
          message: "new result added",
          data: savedResult
        });
      savedResult.lab = req.lab;
      const sendResult = await messenger.informClient(
        req.body.clientMobileNumber,
        savedResult
      );

      savedResult.lab = req.lab._id; // to remove extra lab info from user response.
      res.json({
        done: true,
        message: "new result added and client informed.",
        data: savedResult
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          ServiceError: { code: 114, custom: false, status: 500 },
          AccountLimitError: { code: 122, custom: false, status: 402 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  /*
		This method finds all the results of the given lab.
		The lab is in req.lab.
	*/
  const findLabResults = async (req, res) => {
    try {
      const query = req.query;
      query.lab = req.lab._id;
      const results = await resultService.find(query);
      res.json(results);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 115, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  /*
		This method updates the result specified by resultId (req.params.resultId).
		The result must be a result of the specified lab.
	*/
  const updateResult = async (req, res) => {
    try {
      const newResult = await resultService.update(
        req.params.resultId,
        req.body
      );

      if (!newResult)
        return res
          .status(404)
          .json({ error: errorService.getDefaultError(118) });
      else res.json(newResult);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 117, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // returns the list of all results
  // TODO: remove this functionality later. as soon as the results become more than 100.
  const findAllResults = async (req, res) => {
    try {
      const results = await resultService.find(req.query);
      res.json(results);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method finds the Result with the given id and returns it.
  const findOneResult = async (req, res) => {
    try {
      const result = await resultService.findById(req.params.resultId, "lab");
      if (!result)
        return res
          .status(404)
          .json({ error: errorService.getDefaultError(404) });
      const {
        clientMobileNumber,
        createdAt,
        updatedAt,
        createdBy,
        ...rest
      } = result.toObject();
      if (result) res.json(rest);
      else res.status(404).json({ error: errorService.getDefaultError(404) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method deletes the Lab specified by labId.
  const deleteResult = async (req, res) => {
    try {
      const result = await resultService.deleteById(req.params.resultId);
      if (result)
        res.json({
          done: true,
          message: `Result with id ${req.params.resultId} removed.`
        });
      else res.status(404).json({ error: errorService.getDefaultError(404) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method removes all results from db.
  const clearAll = async (req, res) => {
    try {
      await resultService.deleteMany({});
      res.json({
        done: true,
        message: "All results removed from database! Are you satisfied?"
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  return {
    add: addResult,
    findLabResults,
    findAll: findAllResults,
    findOne: findOneResult,
    update: updateResult,
    delete: deleteResult,
    clearAll
  };
};
