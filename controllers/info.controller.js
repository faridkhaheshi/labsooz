module.exports = ({ redisClient, params, errorService }) => {
  const { appInfoRedisKey } = params.app;

  const getAppInfo = async (req, res) => {
    try {
      const appInfoStr = await redisClient.getAsync(appInfoRedisKey);
      const appInfo = JSON.parse(appInfoStr);
      res.json(appInfo);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {}
      });
      res.status(status).json({ error: userError });
    }
  };

  const setAppInfo = async (req, res) => {
    try {
      const appInfo = req.body;
      await redisClient.setAsync(appInfoRedisKey, JSON.stringify(appInfo));

      res.json({
        done: true,
        message: "App info updated."
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {}
      });
      res.status(status).json({ error: userError });
    }
  };

  return {
    getAppInfo,
    setAppInfo
  };
};
