module.exports = ({ User, Candidate, userService, errorService, tools }) => {
  // This method creates a new User using the data provided through the req.body and saves it in db.
  const createUser = async (req, res) => {
    try {
      const userData = req.body || {};
      userService.normalizeUserData(userData);
      const user = await userService.getOrCreateUser(userData);
      res.json(user);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method finds all Users in the db and returns them in the response.
  const findAllUsers = async (req, res) => {
    try {
      const users = await userService.find(req.query);
      res.json(users);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const findCandidates = async (req, res) => {
    try {
      const cands = await userService.findCandidates(req.query);
      res.json(cands);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const addCandidate = async (req, res) => {
    try {
      const cand = await userService.addCandidate(req.body);
      res.json(cand);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const deactivateCandidate = async (req, res) => {
    try {
      const cand = await userService.editCandidate(req.body, { active: false });
      res.json(cand);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const clearCandidates = async (req, res) => {
    try {
      await userService.deleteManyCandidates({});
      res.json({
        done: true,
        message: "All candidates removed from database! Are you satisfied?"
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method finds the User with the given id and returns it.
  const findOneUser = async (req, res) => {
    try {
      const { populate } = req.query || {};
      if (populate) {
        const user = await userService.findById(
          req.params.userId,
          "labs primaryLab"
        );
        if (user) res.json(user);
        else res.status(404).json({ error: errorService.getDefaultError(404) });
      } else {
        const user = await userService.findById(req.params.userId);
        if (user) res.json(user);
        else res.status(404).json({ error: errorService.getDefaultError(404) });
      }
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method updates the User with the given userId using the data provided in req.body.
  const updateUser = async (req, res) => {
    try {
      const user = await userService.update(req.params.userId, req.body);

      if (!user)
        return res
          .status(404)
          .json({ error: errorService.getDefaultError(404) });
      else res.json(user);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const setPrimaryLab = async (req, res) => {
    try {
      const { primaryLab } = req.body;
      const user = await userService.update(req.params.userId, { primaryLab });

      if (!user)
        return res
          .status(404)
          .json({ error: errorService.getDefaultError(404) });
      else res.json(user);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method deletes the User specified by userId.
  const deleteUser = async (req, res) => {
    try {
      const user = await userService.deleteById(req.params.userId);
      if (user)
        res.json({
          done: true,
          message: `User with id ${req.params.userId} removed.`
        });
      else res.status(404).json({ error: errorService.getDefaultError(404) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method removes all Users from db.
  const clearAll = async (req, res) => {
    try {
      await userService.deleteMany({});
      res.json({
        done: true,
        message: "All users removed from database! Are you satisfied?"
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  return {
    create: createUser,
    findAll: findAllUsers,
    findCandidates,
    addCandidate,
    clearCandidates,
    deactivateCandidate,
    findOne: findOneUser,
    update: updateUser,
    setPrimaryLab,
    delete: deleteUser,
    clearAll
  };
};
