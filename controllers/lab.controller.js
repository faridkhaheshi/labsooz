module.exports = ({
  Lab,
  userService,
  errorService,
  labService,
  imageService,
  params
}) => {
  const labParams = params.lab;
  /*
		This method adds users to the specified lab.
	*/
  const addUserToLab = async (req, res) => {
    try {
      const user = req.body;
      const lab = req.lab;
      const updatedLab = await labService.addUserToLab({ lab, user });
      res.json({
        done: true,
        message: "The user successfully added to the lab",
        data: updatedLab
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 },
          AccountLimitError: { code: 113, custom: false, status: 402 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const getLabUser = async (req, res) => {
    try {
      const { lab } = req;
      const { userId } = req.params;
      const { users: labUsers = [] } = lab;
      if (labUsers.includes(userId)) {
        const theUser = await userService.findById(userId);
        const { labs, type, primaryLab, ...exportedUser } = theUser.toObject();
        res.json(exportedUser);
      } else {
        res.status(404).send({ error: errorService.getDefaultError(404) });
      }
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method removes the given user (req.params.userId) from the lab (req.lab)
  const removeUserFromLab = async (req, res) => {
    try {
      const updatedLab = await labService.removeUserFromLab({
        lab: req.lab,
        userId: req.params.userId
      });
      res.json({
        done: true,
        message: `The user ${req.params.userId} is no longer a user of your lab.`,
        data: updatedLab
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method creates a new Lab using the data provided through the req.body and saves it in db.
  const createLab = async (req, res) => {
    try {
      const labData = req.body || {};
      const creator = req.user;

      if (!labData.owner) labData.owner = creator._id;
      labData.createdBy = creator._id;
      const newLab = await labService.create(labData);
      const newUser = await userService.assignPrimaryLabToUser({
        userId: creator._id,
        labId: newLab._id
      });
      res.json(newLab);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method finds all Labs in the db and returns it in the response.
  const findAllLabs = async (req, res) => {
    try {
      const labs = await labService.find(req.query);
      res.json(labs);
    } catch (error) {
      // console.log(error);
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const findLabMiddleware = async (req, res, next) => {
    try {
      const lab = await labService.findOne({
        _id: req.params.labId,
        active: true
      });
      if (lab) {
        req.lab = lab;
        next();
      } else
        return res
          .status(404)
          .json({ error: errorService.getDefaultError(111) });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 111, custom: true, status: 404 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const findUserLabs = async (req, res) => {
    try {
      const query = { owner: req.user._id, active: true };
      const labs = await labService.find(query);
      res.json(labs);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method finds the Lab with the given id and returns it.
  const findOneLab = async (req, res) => {
    if (req.lab) res.json(req.lab);
    else
      return res.status(404).json({ error: errorService.getDefaultError(111) });
  };

  // This method updates the Lab with the given labId using the data provided in req.body.
  const updateLab = async (req, res) => {
    try {
      const updatedLab = await labService.update(req.params.labId, req.body);
      res.json(updatedLab);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 112, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const deactivateLab = async (req, res) => {
    try {
      const updatedLab = await labService.update(req.params.labId, {
        active: false
      });
      res.json(updatedLab);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 112, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  const reactivateLab = async (req, res) => {
    try {
      const updatedLab = await labService.update(req.params.labId, {
        active: true
      });
      res.json(updatedLab);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 119, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method deletes the Lab specified by labId.
  const deleteLab = async (req, res) => {
    try {
      const lab = await labService.deleteById(req.lab._id);
      res.json({
        done: true,
        message: `Lab with id ${req.lab._id} removed.`
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method removes all labs from db.
  const clearAll = async (req, res) => {
    try {
      const result = await labService.deleteMany({});
      res.json({
        done: true,
        message: "All labs removed from database! Are you satisfied?"
      });
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ValidationError: { code: 109, custom: true, status: 400 },
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  /*
		This method gives the lab user the data required to upload a file to cloudinary.
	*/
  const getUploadPermission = (req, res) => {
    try {
      const uploadData = imageService.permitUpload();
      res.json(uploadData);
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          ServiceError: { code: 116, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  return {
    create: createLab,
    findAll: findAllLabs,
    findOne: findOneLab,
    findUserLabs,
    findLabMiddleware,
    update: updateLab,
    deactivate: deactivateLab,
    reactivate: reactivateLab,
    delete: deleteLab,
    addUser: addUserToLab,
    getLabUser,
    getUploadPermission,
    removeUser: removeUserFromLab,
    clearAll
  };
};
