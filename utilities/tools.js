const PNF = require("google-libphonenumber").PhoneNumberFormat;
const phoneUtil = require("google-libphonenumber").PhoneNumberUtil.getInstance();
const { ValidationError } = require("../utilities/my-errors");

module.exports = (generalParams, path) => {
  function resizePhoto(url, config) {
    let dirAddress = path.dirname(url);
    let fileName = path.basename(url);
    return path.join(dirAddress, config, fileName);
  }

  function createOgTags(result) {
    return {
      url: generalParams.photo.ogUrl,
      title: generalParams.photo.ogTitle,
      image: resizePhoto(result.photoUrl, generalParams.photo.thumbnailConfig),
      description: result.lab.name,
      type: generalParams.photo.ogType
    };
  }

  /*
		if the inpit is a valid mobile number string, the output will include 
		the info about it in this form:
		{
			standardFormat: '+989351269741',
			countryCode: 98,
			isForeign: false,
			nationalNumber: 9351269741
		}
	*/
  const parseMobileNumber = someString => {
    if (typeof someString === "string") {
      try {
        const number = phoneUtil.parseAndKeepRawInput(someString, "IR");
        if (phoneUtil.isValidNumber(number)) {
          const countryCode = number.getCountryCode();
          const nationalNumber = number.getNationalNumber();
          return {
            standardFormat: phoneUtil.format(number, PNF.E164),
            countryCode,
            isForeign: countryCode != "98",
            nationalNumber
          };
        }
      } catch (error) {
        return null;
      }
    }
    return null;
  };

  /*
		This method converts a mobile number to the standard format.
		returns a string.
		if not a valid mobile number, throws an error in the following form:
		{
			type: 'wrong-input'
			message: 'wrong mobile number',
			code: 102
		}
	*/
  const normalizeMobileNumber = mobNumber => {
    try {
      const mobileNumber = parseMobileNumber(mobNumber);
      if (!mobileNumber)
        throw new ValidationError(`${mobNumber} is not a valid mobile number`);

      return mobileNumber.standardFormat;
    } catch (error) {
      if (error.type === "ValidationError") throw error;
      else
        throw new ValidationError(
          error.message || `${mobNumber} is not a valid mobile number`
        );
    }
  };

  const replaceWords = (str, mapObj) => {
    const re = new RegExp(Object.keys(mapObj).join('|'), 'g');
    const newStr = str.replace(re, matched => (mapObj[matched]));
    return newStr;
  };

  return { createOgTags, parseMobileNumber, normalizeMobileNumber, replaceWords };
};
