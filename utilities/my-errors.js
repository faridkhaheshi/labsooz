// Here, we define the fields that we want to have in the JSON error object.
const exportToJSON = error => {
  return {
    type: error.type,
    message: error.message,
    stackTrace: error.stack
  };
};

// This method returns the fields of the error that we want to pass to users.
const exportToUser = error => {
  return {
    type: error.type,
    message: error.message
  };
};

class ValidationError extends Error {
  constructor(message) {
    super(message);
    this.type = "ValidationError";
    this.message = message;
  }

  toJSON = () => exportToJSON(this);

  toUser = () => exportToUser(this);
}

class PermissionError extends Error {
  constructor(message) {
    super(message);
    this.type = "PermissionError";
    this.message = message;
  }

  toJSON = () => exportToJSON(this);

  toUser = () => exportToUser(this);
}

class DatabaseError extends Error {
  constructor(message) {
    super(message);
    this.type = "DatabaseError";
    this.message = message;
  }

  toJSON = () => exportToJSON(this);

  toUser = () => exportToUser(this);
}

class WrongInputError extends Error {
  constructor(message) {
    super(message);
    this.type = "WrongInputError";
    this.message = message;
  }

  toJSON = () => exportToJSON(this);

  toUser = () => exportToUser(this);
}

class ServiceError extends Error {
  constructor(message) {
    super(message);
    this.type = "ServiceError";
    this.message = message;
  }

  toJSON = () => exportToJSON(this);

  toUser = () => exportToUser(this);
}

class UnknownError extends Error {
  constructor(message) {
    super(message);
    this.type = "UnknownError";
    this.message = message;
  }

  toJSON = () => exportToJSON(this);

  toUser = () => exportToUser(this);
}

class AccountLimitError extends Error {
  constructor(message) {
    super(message);
    this.type = "AccountLimitError";
    this.message = message;
  }

  toJSON = () => exportToJSON(this);

  toUser = () => exportToUser(this);
}

module.exports = {
  ValidationError,
  PermissionError,
  DatabaseError,
  WrongInputError,
  ServiceError,
  UnknownError,
  AccountLimitError
};
