module.exports = mongoose => {
  const CandidateSchema = mongoose.Schema(
    {
      mobileNumber: { type: String, required: true },
      active: { type: Boolean, required: true, default: true }
    },
    { timestamps: true }
  );
  return mongoose.model("Candidate", CandidateSchema);
};
