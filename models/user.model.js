module.exports = ({ mongoose, params }) => {
  const Schema = mongoose.Schema;
  const UserSchema = mongoose.Schema(
    {
      mobileNumber: { type: String, required: true },
      email: { type: String, required: false },
      firstName: { type: String, required: false },
      lastName: { type: String, required: false },
      primaryLab: { type: Schema.Types.ObjectId, ref: "Lab", required: false },
      labs: [{ type: Schema.Types.ObjectId, ref: "Lab", required: false }],
      isVerified: { type: Boolean, default: false },
      type: {
        type: String,
        enum: [
          params.userTypes.normal,
          params.userTypes.admin,
          params.userTypes.owner
        ],
        default: params.userTypes.normal
      }
    },
    { timestamps: true }
  );
  return mongoose.model("User", UserSchema);
};
