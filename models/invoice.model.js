module.exports = ({ mongoose, params }) => {
  const Schema = mongoose.Schema;
  const InvoiceSchema = mongoose.Schema(
    {
      lab: { type: Schema.Types.ObjectId, ref: "Lab", required: true },
      credit: {
        type: Number,
        validate: {
          validator: Number.isInteger,
          message: "{VALUE} is not an integer value"
        },
        default: 0
      },
      trialCredit: {
        type: Number,
        validate: {
          validator: Number.isInteger,
          message: "{VALUE} is not an integer value"
        },
        default: 0
      },
      createdBy: { type: Schema.Types.ObjectId, ref: "User", required: true },
      price: {
        type: Number,
        required: true
      },
      status: {
        type: String,
        enum: [params.invoiceStates.unpaid, params.invoiceStates.paid],
        default: params.invoiceStates.unpaid
      },
      isValid: { type: Boolean, default: true }
    },
    { timestamps: true }
  );
  return mongoose.model("Invoice", InvoiceSchema);
};
