module.exports = mongoose => {
  const Schema = mongoose.Schema;
  const ResultSchema = Schema(
    {
      createdBy: { type: Schema.Types.ObjectId, ref: "User", required: true },
      lab: { type: Schema.Types.ObjectId, ref: "Lab", required: true },
      photoUrl: { type: String, required: false },
      photos: [{ type: String, required: true }],
      clientMobileNumber: String
    },
    { timestamps: true }
  );
  return mongoose.model("Result", ResultSchema);
};
