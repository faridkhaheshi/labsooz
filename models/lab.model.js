module.exports = mongoose => {
  const Schema = mongoose.Schema;
  const LabSchema = mongoose.Schema(
    {
      createdBy: { type: Schema.Types.ObjectId, ref: "User", required: true },
      owner: { type: Schema.Types.ObjectId, ref: "User", required: true },
      name: { type: String, required: true },
      trialCredit: {
        type: Number,
        required: true,
        default: 20,
        validate: {
          validator: Number.isInteger,
          message: "{VALUE} is not an integer value"
        }
      },
      credit: {
        type: Number,
        required: true,
        default: 0,
        validate: {
          validator: Number.isInteger,
          message: "{VALUE} is not an integer value"
        }
      },
      resultsSent: {
        type: Number,
        required: true,
        default: 0,
        validate: {
          validator: Number.isInteger,
          message: "{VALUE} is not an integer value"
        }
      },
      logoUrl: String,
      description: String,
      active: { type: Boolean, required: true, default: true },
      users: [{ type: Schema.Types.ObjectId, ref: "Users", required: false }]
    },
    { timestamps: true }
  );

  LabSchema.virtual("remainingCredit").get(function() {
    return this.credit + this.trialCredit - this.resultsSent;
  });

  return mongoose.model("Lab", LabSchema);
};
