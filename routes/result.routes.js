module.exports = ({ app, resultController, authController }) => {
  // View the result
  // STANDARD
  // app.get("/lab/results/:resultId", resultController.showResult);

  // Retrieve all Results
  // STANDARD
  app.get(
    "/api/results",
    authController.validateToken,
    authController.restrictToAdmin,
    resultController.findAll
  );

  // Retrieve a single Result with resultId
  // STANDARD
  // TODO: require token here after implementing one time token for user.
  app.get("/api/results/:resultId", resultController.findOne);

  // Update a Result with resultId
  // STANDARD
  app.put(
    "/api/results/:resultId",
    authController.validateToken,
    authController.restrictToAdmin,
    resultController.update
  );

  // Clear all results from database. THIS IS A VERY DANGEROUS API. REMOVE IT LATER
  // STANDARD
  app.delete(
    "/api/results/clear",
    authController.validateToken,
    authController.restrictToAppOwner,
    resultController.clearAll
  );

  // Delete a Result with resultId
  // STANDARD
  app.delete(
    "/api/results/:resultId",
    authController.validateToken,
    authController.restrictToAppOwner,
    resultController.delete
  );
};
