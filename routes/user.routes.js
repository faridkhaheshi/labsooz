module.exports = ({ app, userController, authController }) => {
  // Retrieve all candidates
  app.get(
    "/api/candidates",
    authController.validateToken,
    authController.restrictToAppOwner,
    userController.findCandidates
  );

  // adds a candidate
  app.post(
    "/api/candidates",
    authController.validateToken,
    authController.restrictToAppOwner,
    userController.addCandidate
  );

  // remove a candidate from whitelist
  app.delete(
    "/api/candidates",
    authController.validateToken,
    authController.restrictToAppOwner,
    userController.deactivateCandidate
  );

  // remove a candidate from whitelist
  app.delete(
    "/api/candidates/clear",
    authController.validateToken,
    authController.restrictToAppOwner,
    userController.clearCandidates
  );

  // Create a new User
  app.post(
    "/api/users",
    authController.validateToken,
    authController.restrictToAppOwner,
    userController.create
  );

  // Retrieve all Users
  // STANDARD
  app.get(
    "/api/users",
    authController.validateToken,
    authController.restrictToAdmin,
    userController.findAll
  );

  // Retrieve a single User with userId
  // STANDARD
  app.get(
    "/api/users/:userId",
    authController.validateToken,
    authController.restrictToAdminOrSelf,
    userController.findOne
  );

  // Update a User with userId
  // STANDARD
  app.put(
    "/api/users/:userId",
    authController.validateToken,
    authController.restrictToAdmin,
    userController.update
  );

  app.put(
    "/api/users/:userId/primaryLab",
    authController.validateToken,
    authController.restrictToAdminOrSelf,
    userController.setPrimaryLab
  );

  // Clear all Users from database. THIS IS A VERY DANGEROUS API. REMOVE IT LATER
  // STANDARD
  app.delete(
    "/api/users/clear",
    authController.validateToken,
    authController.restrictToAppOwner,
    userController.clearAll
  );

  // Delete a User with userId
  // STANDARD
  app.delete(
    "/api/users/:userId",
    authController.validateToken,
    authController.restrictToAppOwner,
    userController.delete
  );
};
