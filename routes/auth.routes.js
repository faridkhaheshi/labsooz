module.exports = ({ app, authController }) => {
  // These are the "unprotected" routes.

  // get otp
  // TODO: prevent multiple requests from the same person.
  // STANDARD
  app.post(
    "/auth/otp",
    authController.validateUser,
    authController.restrictToWhitelist,
    authController.askForOtp
  );

  // login
  // STANDARD
  app.post(
    "/auth/login",
    authController.validateUser,
    authController.restrictToWhitelist,
    authController.login
  );
};
