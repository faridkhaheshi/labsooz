module.exports = ({ app, labController, authController, resultController }) => {
  // Create a new Lab
  // STANDARD
  app.post(
    "/api/labs",
    authController.validateToken,
    authController.restrictToCandidates,
    labController.create
  );

  // Retrieve user labs
  // STANDARD
  app.get(
    "/api/labs",
    authController.validateToken,
    labController.findUserLabs
  );

  // retrieves all labs.
  app.get(
    "/api/labs/all",
    authController.validateToken,
    authController.restrictToAdmin,
    labController.findAll
  );

  // Retrieve a single Lab with labId
  // STANDARD
  app.get(
    "/api/labs/:labId",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabOwnerOrAdmin,
    labController.findOne
  );

  // Update a Lab with labId
  // STANDARD
  app.put(
    "/api/labs/:labId",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabOwnerOrAdmin,
    labController.update
  );

  app.put(
    "/api/labs/:labId/deactivate",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabOwnerOrAdmin,
    labController.deactivate
  );

  app.put(
    "/api/labs/:labId/reactivate",
    authController.validateToken,
    authController.restrictToAdmin,
    labController.reactivate
  );

  // Clear all labs from database. THIS IS A VERY DANGEROUS API. REMOVE IT LATER
  // STANDARD
  app.delete(
    "/api/labs/clear",
    authController.validateToken,
    authController.restrictToAppOwner,
    labController.clearAll
  );

  // Delete a Lab with LabId
  // STANDARD
  app.delete(
    "/api/labs/:labId",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToAppOwner,
    labController.delete
  );

  // Add user to lab
  // STANDARD
  app.post(
    "/api/labs/:labId/users",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabOwnerOrAdmin,
    labController.addUser
  );

  // returns the lab user specified by userId
  app.get(
    "/api/labs/:labId/users/:userId",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabOwnerOrAdmin,
    labController.getLabUser
  );

  // Removes a user from the lab
  // STANDARD
  app.delete(
    "/api/labs/:labId/users/:userId",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabOwner,
    labController.removeUser
  );

  // Adds a result for the lab specified by labId.
  // STANDARD
  app.post(
    "/api/labs/:labId/results",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabUsers,
    resultController.add
  );

  // Get all the results of the lab specified by labId
  // STANDARD
  app.get(
    "/api/labs/:labId/results",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabUsers,
    resultController.findLabResults
  );
  // Get the data required to upload an image to server (cloudinary)
  // STANDARD
  app.get(
    "/api/labs/:labId/upload",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabUsers,
    labController.getUploadPermission
  );

  // Update the result specified by resultId. The result must be for labId.
  // STANDARD
  app.put(
    "/api/labs/:labId/results/:resultId",
    authController.validateToken,
    labController.findLabMiddleware,
    authController.restrictToLabUsers,
    resultController.update
  );
};
