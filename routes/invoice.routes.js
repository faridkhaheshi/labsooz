module.exports = ({ app, authController, invoiceController }) => {
  app.get(
    "/api/invoices",
    authController.validateToken,
    authController.restrictToAppOwner,
    invoiceController.find
  );

  app.post(
    "/api/invoices",
    authController.validateToken,
    authController.restrictToAppOwner,
    invoiceController.add
  );

  app.delete(
    "/api/invoices/clear",
    authController.validateToken,
    authController.restrictToAppOwner,
    invoiceController.clearAll
  );

  app.put(
    "/api/invoices/:invoiceId/cancel",
    authController.validateToken,
    authController.restrictToAppOwner,
    invoiceController.cancel
  );

  app.put(
    "/api/invoices/:invoiceId/pay",
    authController.validateToken,
    authController.restrictToAppOwner,
    invoiceController.pay
  );
};
