module.exports = ({ app, indexHtml, params, tools }) => {
  const { defaultTags = {} } = params;

  app.use("/lab", (req, res, next) => {
    req.tags = {
      __APPLE_TOUCH_ICON__: 'logo192.png',
      __TITLE__: 'Send Medical Test Results Electronically | Lab for Medical Labs',
      __DESCRIPTION__: 'Medical labs can send test results electronically to their patients using Lab software.',
      __FAVICON__: '/lab-favicon.ico',
      __THEME_COLOR__: '#990099',
      __OG_URL__: 'https://bistopanj.com/lab',
      __OG_TITLE__: 'Send Medical Test Results Electronically | Lab for Medical Labs',
      __OG_IMAGE__: 'https://bistopanj.com/lab_customer_fa.jpg',
      __OG_IMAGE_ALT__: 'A patient receives her medical test result on her mobile phone.',
      __OG_DESCRIPTION__: 'Medical labs can send test results electronically to their patients using Lab software.',
      __OG_SITE_NAME__: 'Lab',
      __TWITTER_URL__: 'https://bistopanj.com/lab',
      __TWITTER_TITLE__: 'Send Medical Test Results Electronically | Lab for Medical Labs',
      __TWITTER_DESCRIPTION__: 'Medical labs can send test results electronically to their patients using Lab software.',
      __TWITTER_IMAGE__: 'https://bistopanj.com/lab_customer_fa.jpg',
      __TWITTER_IMAGE_ALT__: 'A patient receives her medical test result on her mobile phone.',
      __APPLE_MOB_WEB_APP_TITLE__: 'Lab',
    };
    next();
  });

  app.use("/lab/result/:resultId", (req, res, next) => {
    req.tags = {
      ...req.tags,
      __TITLE__: 'Your test result is ready',
      __OG_TITLE__: 'Your test result is ready',
      __OG_IMAGE_ALT__: 'A patient receives her medical test result on her mobile phone.',
      __OG_DESCRIPTION__: 'The laboratory has sent your test result.',
      __TWITTER_TITLE__: 'Your test result is ready',
      __TWITTER_DESCRIPTION__: 'The laboratory has sent your test result.',
    };
    next();
  });

  app.get("*", (req, res) => {
    const tags = req.tags || defaultTags;
    const updatedHtml = tools.replaceWords(indexHtml, tags);
    res.send(updatedHtml);
  });
};
