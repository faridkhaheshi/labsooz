module.exports = ({ app, authController, infoController, mobileApps }) => {
  app.get(
    "/api/info/latest-app-version",
    authController.validateToken,
    infoController.getAppInfo
  );

  app.post(
    "/api/info/latest-app-version",
    authController.validateToken,
    authController.restrictToAppOwner,
    infoController.setAppInfo
  );
};
