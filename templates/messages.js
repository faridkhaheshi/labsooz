module.exports = {
  resultPage: {
    headTitleDefault: "نتیجه‌ی آزمایش",
    headTitle: "{{labName}} | نتیجه‌ی آزمایش",
    bodyTitle: "نتیجه‌ی آزمایش",
    notFoundMessage: "آزمایش مورد نظر پیدا نشد.",
    internalProblemMessage:
      "در حال حاضر قادر به انجام خواسته‌ی شما نیستیم. بابت مشکل به وجود آمده از شما پوزش می‌طلبیم.",
    problemTitle: "مشکلی پیش آمد"
  },
  sms: {
    resultSMS: `{{labName}}

نتیجه‌ی آزمایش شما:
{{resultId}}
فعلا چون نمی تونیم لینک رو بفرستیم فقط آی دی ش رو می‌فرستیم. ولی در واقع این جا لینک به جواب آزمایشه.

`,
    otpSMS: `{{otp}}
رمز یک بار مصرف شما برای ورود به لب سوز.`
  }
};

/*

module.exports = {
  resultPage: {
    headTitleDefault: "نتیجه‌ی آزمایش",
    headTitle: "{{labName}} | نتیجه‌ی آزمایش",
    bodyTitle: "نتیجه‌ی آزمایش",
    notFoundMessage: "آزمایش مورد نظر پیدا نشد.",
    internalProblemMessage:
      "در حال حاضر قادر به انجام خواسته‌ی شما نیستیم. بابت مشکل به وجود آمده از شما پوزش می‌طلبیم.",
    problemTitle: "مشکلی پیش آمد"
  },
  sms: {
    resultSMS: `{{labName}}

نتیجه‌ی آزمایش شما:
https://bistopanj.com/lab/result/{{resultId}}`,
    otpSMS: `{{otp}}
رمز یک بار مصرف شما برای ورود به لب سوز.`
  }
};

*/
