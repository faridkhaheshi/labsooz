const config = require("config");
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const fs = require("fs");
const mongoose = require("mongoose");
const mustache = require("mustache");
const Promise = require("bluebird");
const jwt = require("jsonwebtoken");
const redis = require("redis");
const cloudinary = require("cloudinary");
const cors = require("cors");
const smsConfig = config.get("smsConfig");
const jwtSecret =
  process.env.JWT_SECRET || config.get("deploymentConfig").jwtSecret;
const appConfig = config.get("appConfig");
const mobileApps = config.get("mobileApps");
const imageServerConfig = config.get("imageServerConfig");
const generalParams = require("./parameters");
const errorService = require("./services/error.service")({
  errorCodes: generalParams.errorCodes,
  errorTypes: generalParams.errorTypes
});

Promise.promisifyAll(redis.RedisClient.prototype);
Promise.promisifyAll(redis.Multi.prototype);
const redisUri =
  process.env.REDIS_URL || config.get("deploymentConfig").redisUri;
redisClient = redis.createClient(redisUri);
redisClient.on("error", error => console.log("Error " + error));
// TODO: make sure that we have a working redis. otherwise, stop the app.
const interfaceMessages = require("./templates/messages");
const smsServices = require("./services/sms-services")(smsConfig);
const messenger = require("./services/messenger.service")(
  smsServices,
  interfaceMessages,
  mustache
);
const otpService = require("./services/otp.service")({
  params: generalParams,
  messenger,
  redisClient
});
const tools = require("./utilities/tools")(generalParams, path);

const appOwnerMobileNumber =
  process.env.APP_OWNER_MOBILE_NUMBER ||
  config.get("appConfig").appOwnerMobileNumber;
mongoose.Promise = global.Promise;

// Connecting to mongodb
const dbUrl = process.env.MONGODB_URI || config.get("deploymentConfig").dbUrl;
mongoose.set("useFindAndModify", false);
mongoose
  .connect(dbUrl, { useNewUrlParser: true })
  .then(() => console.log("successfully connected to mongodb"))
  .catch(error => {
    console.log("Could not connect to the mongodb. Exiting now...", error);
    process.exit();
  });

// Declaring mongodb Models
const Lab = require("./models/lab.model")(mongoose);
const Result = require("./models/result.model")(mongoose);
const User = require("./models/user.model")({
  mongoose,
  params: generalParams
});
const Invoice = require("./models/invoice.model")({
  mongoose,
  params: generalParams
});
const Candidate = require("./models/candidate.model")(mongoose);

const userService = require("./services/user.service")({
  User,
  Candidate,
  tools,
  params: generalParams,
  appOwnerMobileNumber
});
const labService = require("./services/lab.service")({
  Lab,
  User,
  userService,
  params: generalParams
});
const resultService = require("./services/result.service")({
  Result,
  tools,
  labService,
  params: generalParams
});
const invoiceService = require("./services/invoice.service")({
  Invoice,
  mongoose,
  Lab,
  params: generalParams
});
const tokenService = require("./services/token.service")({
  jwt,
  jwtSecret,
  params: generalParams.token
});
const imageService = require("./services/image.service")({
  cloudinary,
  imageServerConfig
});

// Setting up the web-server
const port = process.env.PORT || config.get("deploymentConfig").defaultPort;
const app = express();

// Defining Controllers
const labController = require("./controllers/lab.controller")({
  Lab,
  userService,
  errorService,
  labService,
  imageService,
  params: generalParams
});
const resultController = require("./controllers/result.controller")({
  Result,
  interfaceMessages,
  messenger,
  mustache,
  tools,
  resultService,
  errorService,
  params: generalParams
});
const userController = require("./controllers/user.controller")({
  User,
  Candidate,
  userService,
  errorService,
  tools
});
const authController = require("./controllers/auth.controller")({
  otpService,
  tools,
  userService,
  labService,
  tokenService,
  errorService,
  appOwnerMobileNumber,
  params: generalParams
});
const invoiceController = require("./controllers/invoice.controller")({
  Invoice,
  errorService,
  invoiceService
});
const infoController = require("./controllers/info.controller")({
  redisClient,
  errorService,
  params: generalParams
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// app.set('view engine', 'pug');
const indexHtml = fs.readFileSync(
  path.join(__dirname, "client", "index.html"),
  "utf8"
);
console.log("index.html loaded to memory");

app.use("/", express.static(path.join(__dirname, "client", "public")));

app.use(cors());

// app.get('/', (req, res) => res.send('Hello World!'));

// Handle Routes using Controllers
require("./routes/lab.routes")({
  app,
  labController,
  authController,
  resultController
});
require("./routes/result.routes")({
  app,
  resultController,
  authController
});
require("./routes/user.routes")({
  app,
  userController,
  authController
});
require("./routes/auth.routes")({
  app,
  authController
});

require("./routes/info.routes")({
  app,
  authController,
  infoController,
  mobileApps
});

require("./routes/invoice.routes")({
  app,
  authController,
  invoiceController
});

require("./routes/web.routes")({
  app,
  indexHtml,
  params: generalParams,
  tools
});

// app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'public', 'index.html')));
// app.get('*', (req, res) => res.send(indexHtml));

app.listen(port, () => console.log(`App listening on port ${port}!`));
