const { errorTypes, errorCodes } = require("./error-codes");

const reviewers = {
  "+14089961010": { name: "apple", otp: "1010" },
  "+12125650000": { name: "google", otp: "9999" }
};

const defaultTags = {
  __TITLE__: "Bistopanj | Simple Internet Tools for Businesses",
  __DESCRIPTION__:
    "We create simple internet tools for businesses. Tools that can be setup fast and easy. No waiting time. No hidden cost.",
  __FAVICON__: "/favicon.ico",
  __THEME_COLOR__: "#1f28de",
  __APPLE_TOUCH_ICON__: "logo192.png",
  __OG_URL__: "https://bistopanj.com",
  __OG_TITLE__: "Bistopanj | Simple Internet Tools for Businesses",
  __OG_IMAGE__: "https://bistopanj.com/logo512.png",
  __OG_IMAGE_ALT__: "Bistopanj Company Logo",
  __OG_DESCRIPTION__:
    "We create simple internet tools for businesses. Tools that can be setup fast and easy. No waiting time. No hidden cost.",
  __OG_SITE_NAME__: "Bistopanj",
  __TWITTER_URL__: "https://bistopanj.com",
  __TWITTER_TITLE__: "Bistopanj | Simple Internet Tools for Businesses",
  __TWITTER_DESCRIPTION__:
    "We create simple internet tools for businesses. Tools that can be setup fast and easy. No waiting time. No hidden cost.",
  __TWITTER_IMAGE__: "https://bistopanj.com/logo512.png",
  __TWITTER_IMAGE_ALT__: "Bistopanj Company Logo",
  __APPLE_MOB_WEB_APP_TITLE__: "Bistopanj"
};

module.exports = {
  photo: {
    thumbnailConfig: "w_500,h_500,c_fill/dpr_1.0/q_40",
    ogUrl: "http://bistopanj.com",
    ogTitle: "نتیجه‌ی آزمایش شما",
    ogType: "website"
  },
  corsWhiteList: [],
  otp: {
    length: 6,
    expireTime: 60 * 10, // seconds
    redisKeyStart: "OTP"
  },
  token: {
    expiresIn: "30 days",
    issuer: "bistopanj.com",
    clockTolerance: 60
  },
  userTypes: {
    owner: "OWNER",
    admin: "ADMIN",
    normal: "NORMAL"
  },
  invoiceStates: {
    paid: "paid",
    unpaid: "unpaid"
  },
  lab: {
    maxUsers: 5
  },
  app: {
    maxResultsPerQuery: 20,
    appInfoRedisKey: "APP_INFO"
  },
  reviewers,
  errorTypes,
  errorCodes,
  defaultTags
};
