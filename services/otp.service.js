const { ServiceError } = require("../utilities/my-errors");
module.exports = ({ params, messenger, redisClient }) => {
  const otpParams = params.otp;

  /*
        sends a otp to the given mobile number. 
        if done successfully resolves with the data about the otp.
     */
  const sendOtpTo = async mobileNumber => {
    /*
            1. Generate a random n-digit number (OTP).
            2. Save it in redis as { mobileNumber: OTP } with expiration time. 
            3. Send it via SMS to mobileNumber.
        */
    try {
      const otp = Math.floor(
        Math.pow(10, otpParams.length - 1) * (1 + 9 * Math.random())
      );
      await redisClient.setexAsync(
        `${otpParams.redisKeyStart}:${mobileNumber}`,
        otpParams.expireTime,
        otp
      );
      await messenger.sendOtp({ phoneNumber: mobileNumber, otp });
      return { expiresInSeconds: otpParams.expireTime };
    } catch (error) {
      console.error(error);
      if (error.type == params.errorTypes.ServiceError) {
        console.error(
          "[sendOtpTo.otp.service: ] There is some problems with the sms service provider."
        );
        // TODO: do everything you can to solve the problem.
        throw error;
      } else {
        console.error(
          `[sendOtpTo.otp.service: ] Something went wrong when trying to send otp to mobile number ${mobileNumber}`
        );
        throw new ServiceError(
          "Something went wrong in sendOtpTp.otp.service."
        );
      }
    }
  };

  /*
     This method checks to see if the { otp, mobileNumber } pair is valid.
     If yes, resolves with true,
     If not, resolves with false.
    */

  const checkOtp = async ({ otp, mobileNumber }) => {
    try {
      const { reviewers } = params;
      if (mobileNumber in reviewers) return otp === reviewers[mobileNumber].otp;
      const accessKey = `${otpParams.redisKeyStart}:${mobileNumber}`;
      const savedOtp = await redisClient.getAsync(accessKey);
      const isValid = savedOtp == otp;
      if (isValid) await redisClient.delAsync(accessKey);
      return isValid;
    } catch (error) {
      console.error(
        "[checkOtp.otp.service: ] There is some problems with redis."
      );
      console.error(error);

      throw new ServiceError("Something went wrong in checkOtp.otp.service.");
    }
  };

  return {
    sendOtpTo,
    checkOtp
  };
};
