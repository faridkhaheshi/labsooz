const { DatabaseError, ValidationError } = require("../utilities/my-errors");

module.exports = ({ Result, tools, params, labService }) => {
  // This method finds the result by the given id and returns it.
  const findById = async (id, populate = null) => {
    try {
      const result = await Result.findById(id).populate(
        populate,
        "name description logoUrl _id"
      );
      return result;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[findById.result.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside findById.result.service"
        );
      }
    }
  };

  /*
        This method normalizes the result data to a normal form.
        For now, it changes the clientMobileNumber to standard format.
    */
  const normalizeResultData = resultData => {
    if (!(resultData || {}).clientMobileNumber || !(resultData || {}).photos)
      throw new ValidationError(
        "The result data must include a valid clientMobileNumber and photos"
      );
    try {
      const mobileNumber = tools.parseMobileNumber(
        resultData.clientMobileNumber
      );
      resultData.clientMobileNumber = mobileNumber.standardFormat;
    } catch (error) {
      throw new ValidationError("Wrong mobile number.");
    }
  };

  /*
        This method gets an objects and changes its fields to be in the standard format to be put into the db.
    */
  const preProcess = resultData => {
    try {
      const funcMap = {
        clientMobileNumber: tools.normalizeMobileNumber
      };

      Object.keys(funcMap).forEach(key => {
        if (key in resultData) resultData[key] = funcMap[key](resultData[key]);
      });

      return resultData;
    } catch (error) {
      if (error.type === params.errorTypes.ValidationError) throw error;
      else
        throw new ValidationError(
          error.message || "Wrong input provided for a result."
        );
    }
  };

  // this method updates a given result
  const updateResult = async (resultId, newData) => {
    try {
      const updatedData = preProcess(newData);
      const newResult = await Result.findByIdAndUpdate(resultId, updatedData, {
        new: true
      });
      return newResult;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[updateResult.result.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside updateResult.result.service"
        );
      }
    }
  };

  /*
        This method finds the results specified by the given query.
    */
  const find = async query => {
    try {
      const { createdBefore, ...newQuery } = query;
      if (createdBefore) newQuery.createdAt = { $lt: createdBefore };
      const results = await Result.find(newQuery)
        .limit(params.app.maxResultsPerQuery)
        .sort({ createdAt: "descending" })
        .populate("createdBy", "firstName lastName _id mobileNumber")
        .exec();
      return results;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[find.result.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside find.result.service"
        );
      }
    }
  };

  // This method creates a new result and saves it to database.
  const addResult = async ({ result, lab }) => {
    try {
      const { remainingCredit = -1, active = false } = lab || {};
      if (!active || remainingCredit <= 0)
        throw new AccountLimitError("Not enough credit to send the result.");

      const newResult = new Result(result);
      const savedResult = await newResult.save();
      const updatedLab = await labService.update(lab._id, {
        $inc: { resultsSent: 1 }
      });
      return savedResult;
    } catch (error) {
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(
          error.message || "invalid input for creating a result."
        );
      else {
        console.error("[addResult.result.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside addResult.result.service"
        );
      }
    }
  };

  const deleteMany = async query => {
    try {
      const result = await Result.deleteMany(query);
      return result;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "Invalid query parameters.");
      else {
        console.error("[deleteMany.result.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside deleteMany.result.service"
        );
      }
    }
  };

  const deleteById = async resultId => {
    try {
      const result = await Result.findByIdAndRemove(resultId);
      return result;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "Invalid result id.");
      else {
        console.error("[deleteById.result.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside deleteById.result.service"
        );
      }
    }
  };

  return {
    add: addResult,
    find,
    update: updateResult,
    findById,
    normalizeResultData,
    preProcess,
    deleteMany,
    deleteById
  };
};
