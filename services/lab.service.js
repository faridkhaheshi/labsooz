const {
  DatabaseError,
  ValidationError,
  AccountLimitError
} = require("../utilities/my-errors");

module.exports = ({ Lab, User, userService, params }) => {
  const labParams = params.lab;

  /*
        This method looks for a lab with the given id.
        resolves with the lab if found.
        if not found, resolves with null.
        rejects if some error occurs.
    */
  const findById = async labId => {
    try {
      const lab = await Lab.findById(labId);
      return lab;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid lab Id");
      else {
        console.error("[findById.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside findById.lab.service"
        );
      }
    }
  };

  const findOne = async query => {
    try {
      const lab = await Lab.findOne(query);
      return lab;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid query");
      else {
        console.error("[findById.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside findOne.lab.service"
        );
      }
    }
  };

  /*
        This method resolves with the list of labs that meet the given query.
    */
  const findLabs = async query => {
    try {
      const labs = await Lab.find(query)
        .sort({ createdAt: "descending" })
        .exec();
      return labs;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[findLabs.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside findLabs.lab.service"
        );
      }
    }
  };

  /*
        This method updates the lab specified by labId with the new data provided.
        resolves with the updated lab.
    */
  const updateLab = async (labId, newData) => {
    try {
      const updatedLab = await Lab.findByIdAndUpdate(labId, newData, {
        new: true
      });
      return updatedLab;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[updateLab.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside updateLab.lab.service"
        );
      }
    }
  };

  /*
        This method adds the given userId to the list of users of the given lab.
        It also updates the user to include the lab in its labs arrays.

    */
  const addUserToLab = async ({ lab, user }) => {
    /*
			We should:
			1. get the lab info from database. 
			2. get user info or create a user if don't exist.
			3. add user id to the list of users in the lab.
			4. add the lab id to the list of labs in the user.
			5. update both objects in the database.
        */
    if (lab.users.length >= labParams.maxUsers)
      throw new AccountLimitError(
        `Maximum number of users for each lab is ${labParams.maxUsers}`
      );

    try {
      userService.normalizeUserData(user);
      const newUser = await userService.getOrCreateUser(user);

      const [updatedLab, updatedUser] = await Promise.all([
        Lab.findByIdAndUpdate(
          lab._id,
          { $addToSet: { users: newUser._id } },
          { new: true }
        ),
        userService.addLabToUser({ userId: newUser._id, labId: lab._id })
      ]);

      // TODO: send sms to user.

      return updatedLab;
    } catch (error) {
      console.log(error);
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[addUserToLab.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside addUserToLab.lab.service"
        );
      }
    }
  };

  /*
        This method removes the user (specified by userId) from the given lab.
        Resolves with the updated lab.

    */
  const removeUserFromLab = async ({ lab, userId }) => {
    try {
      const [updatedLab, updatedUser] = await Promise.all([
        Lab.findByIdAndUpdate(
          lab._id,
          { $pull: { users: userId } },
          { new: true }
        ),
        userService.removeLabFromUser({ userId, labId: lab._id })
      ]);
      return updatedLab;
    } catch (error) {
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[removeUserFromLab.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside removeUserFromLab.lab.service"
        );
      }
    }
  };

  /*
        This method creates a lab using the given labData and saves it to database.

    */
  const createLab = async labData => {
    try {
      const newLab = new Lab(labData);
      const savedLab = await newLab.save();
      return savedLab;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(
          error.message || "Invalid input parameters to create a lab."
        );
      else {
        console.error("[createLab.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside createLab.lab.service"
        );
      }
    }
  };

  // This method removes the lab specified by query from database.
  const deleteMany = async query => {
    try {
      const result = await Lab.deleteMany(query);
      return result;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "Invalid query parameters.");
      else {
        console.error("[deleteMany.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside deleteMany.lab.service"
        );
      }
    }
  };

  // This method removes the lab specified by the given id.
  const deleteById = async labId => {
    try {
      const lab = await Lab.findByIdAndRemove(labId);
      return lab;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "Invalid lab id.");
      else {
        console.error("[deleteById.lab.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside deleteById.lab.service"
        );
      }
    }
  };

  return {
    find: findLabs,
    findOne,
    findById,
    update: updateLab,
    create: createLab,
    addUserToLab,
    removeUserFromLab,
    deleteMany,
    deleteById
  };
};
