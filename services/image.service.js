const { ValidationError, ServiceError } = require("../utilities/my-errors");
module.exports = ({ cloudinary, imageServerConfig }) => {
  const cloudinaryApiKey = imageServerConfig.apiKey;
  const cloudinaryApiSecret = imageServerConfig.apiSecret;
  const cloudinaryCloudName = imageServerConfig.cloudName;

  permitUpload = () => {
    try {
      const timestamp = ((Date.now() / 1000) | 0).toString();
      const params = { 
        timestamp,
        eager: 'q_100|q_60,w_1600|q_100,w_1600',
        transformation: 'a_exif',
      };
      const signature = cloudinary.utils.api_sign_request(
        params,
        cloudinaryApiSecret
      );
      const uploadUrl = `https://api.cloudinary.com/v1_1/${cloudinaryCloudName}/image/upload`;

      return {
        uploadUrl,
        formDataInfo: {
          ...params,
          api_key: cloudinaryApiKey,
          signature
        }
      };
    } catch (error) {
      console.error("error in permitUpload.image.service:");
      console.error(error);
      throw new ServiceError(
        error.message ||
          "Something went wrong when trying to create upload permission data."
      );
    }
  };

  return { permitUpload };
};
