module.exports = (sms, interfaceMessages, mustache) => {
  function informClient(phoneNumber, resultData) {
    // Todo: Change this. The result should be added to a sending queue.
    // 		Even if something goes wrong with the sms service, the lab operator
    // 		should not wait.
    const options = {
      message: mustache.render(interfaceMessages.sms.resultSMS, {
        labName: resultData.lab.name,
        resultId: resultData._id
      }),
      lineNumber: "3000257076",
      recipient: phoneNumber
    };

    return sms.send(options);
  }

  const sendOtp = ({ phoneNumber, otp }) => {
    const options = {
      message: mustache.render(interfaceMessages.sms.otpSMS, { otp }),
      lineNumber: "3000257076",
      recipient: phoneNumber
    };

    return sms.send(options);
  };

  return { informClient, sendOtp };
};
