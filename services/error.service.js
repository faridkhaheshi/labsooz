module.exports = ({ errorCodes, errorTypes }) => {
  const unknownErrorCode = 999;

  const getDefaultError = code => ({
    code,
    type: errorCodes[code].type,
    message: errorCodes[code].message,
    moreInfo: errorCodes[code].moreInfo
  });

  /*
        This method creates a customized error object for user based on the Error object
        received as parameter.
        It may replace the default error message in errorCodes with the custom message received in 
        error.message.
    */
  const buildCustomError = ({
    code = unknownErrorCode,
    error = {},
    custom = false
  }) => {
    const customError = getDefaultError(code);
    if (custom && error.message) customError.message = error.message;
    return customError;
  };
  
  const buildErrorResponse = ({ error = {}, conditionalMap = {} }) => {
    const errorType = error.type;
    if (errorType in conditionalMap) {
      return {
        status: conditionalMap[errorType]["status"] || 500,
        error: buildCustomError({
          code: conditionalMap[errorType]["code"],
          error,
          custom: conditionalMap[errorType]["custom"]
        })
      };
    } else {
      console.error("Unknown Error: ");
      console.error(error);
      return {
        status: 500,
        error: getDefaultError(unknownErrorCode)
      };
    }
  };

  return {
    buildErrorResponse,
    getDefaultError
  };
};
