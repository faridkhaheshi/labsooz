const {
  DatabaseError,
  ValidationError,
  AccountLimitError,
  WrongInputError
} = require("../utilities/my-errors");

module.exports = ({ Invoice, Lab, params }) => {
  const createInvoice = async invoiceData => {
    try {
      const newInvoice = new Invoice(invoiceData);
      const savedInvoice = await newInvoice.save();
      return savedInvoice;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(
          error.message || "Invalid input parameters to create an invoice."
        );
      else {
        console.error("[createInvoice.invoice.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside createInvoice.invoice.service"
        );
      }
    }
  };

  const findInvoices = async query => {
    try {
      const invoices = await Invoice.find(query)
        .sort({ createdAt: "descending" })
        .exec();
      return invoices;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[findInvoices.invoice.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside findInvoices.invoice.service"
        );
      }
    }
  };

  const deleteMany = async query => {
    try {
      const result = await Invoice.deleteMany(query);
      return result;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "Invalid query parameters.");
      else {
        console.error("[deleteMany.invoice.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside deleteMany.invoice.service"
        );
      }
    }
  };

  const updateInvoice = async (id, newData) => {
    try {
      const updatedInvoice = await Invoice.findByIdAndUpdate(id, newData, {
        new: true
      });
      return updatedInvoice;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[updateInvoice.invoice.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside updateInvoice.invoice.service"
        );
      }
    }
  };

  const payInvoice = async id => {
    // TODO: handle it using mongoose session
    try {
      const inv = await Invoice.findByIdAndUpdate(id, {
        isValid: true,
        status: params.invoiceStates.paid
      });
      if (!inv) throw new WrongInputError("The invoice does not exist");

      const { credit, trialCredit, lab: labId, status, isValid } = inv;

      if (!isValid) throw new WrongInputError("The invoice does not exist");

      if (status === params.invoiceStates.paid)
        throw new WrongInputError("The Invoice is already paid.");
      const updatedLab = await Lab.findByIdAndUpdate(
        labId,
        { $inc: { credit, trialCredit } },
        { new: true }
      );

      return { invoice: inv, lab: updatedLab };
    } catch (error) {
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        throw new DatabaseError(
          error.message || "error happened inside payInvoice.invoice.service"
        );
      }
    }
  };

  return {
    find: findInvoices,
    create: createInvoice,
    deleteMany,
    update: updateInvoice,
    pay: payInvoice
  };
};
