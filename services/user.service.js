const {
  DatabaseError,
  ValidationError,
  UnknownError
} = require("../utilities/my-errors");
module.exports = ({ User, Candidate, tools, params, appOwnerMobileNumber }) => {
  const getOrCreateUser = async (userData = {}, verified = false) => {
    try {
      const { _id = null, mobileNumber = null, ...rest } = userData;
      if (!_id && !mobileNumber)
        throw new ValidationError(
          "Not enough information is provided to specify a user."
        );
      const query = {};
      if (verified) rest.verified = verified;
      if (_id) query._id = _id;
      if (mobileNumber) query.mobileNumber = mobileNumber;
      const user = await User.findOneAndUpdate(query, rest, {
        new: true
      }).populate("primaryLab");
      if (!user) {
        if (verified) userData.isVerified = verified;
        const newUser = new User(userData);
        const createdUser = await newUser.save();
        return createdUser;
      } else {
        return user;
      }
    } catch (error) {
      if (error.type == params.errorTypes.ValidationError) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid user info");
      else {
        console.error("[getOrCreateUser.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside getOrCreateUser.user.service"
        );
      }
    }
  };

  /*
        This method looks for a user with the given id.
        resolves with the user if found.
        if not found, resolves with null.
        rejects if some error occurs.
    */
  const findById = async (userId, populate = "") => {
    try {
      const user = await User.findById(userId).populate(
        populate,
        "_id name description logoUrl"
      );
      return user;
    } catch (error) {
      // TODO: ensure that this is the correct way to handle bad id errors
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid user id");
      else {
        console.error("[findById.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside findById.user.service"
        );
      }
    }
  };

  /*
        This method resolves with the list of users that meet the given query.
    */
  const findUsers = async query => {
    try {
      const users = await User.find(query)
        .sort({ createdAt: "descending" })
        .exec();
      return users;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[findUsers.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside findUsers.user.service"
        );
      }
    }
  };

  const findOne = async query => {
    try {
      const user = await User.findOne(query);
      return user;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[findOne.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside findOne.user.service"
        );
      }
    }
  };

  const findCandidates = async query => {
    try {
      const cands = await Candidate.find(query)
        .sort({ createdAt: "descending" })
        .exec();
      return cands;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[findAllCandidates.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message ||
            "error happened inside findAllCandidates.user.service"
        );
      }
    }
  };

  const isCandidate = async (query = {}) => {
    const { mobileNumber = null } = query;
    if (!mobileNumber) throw new ValidationError("mobileNumber is mandatory");
    try {
      const theMobileNumber = tools.parseMobileNumber(mobileNumber);
      if (!theMobileNumber) throw new ValidationError("mobileNumber is wrong");
      const stdMobileNumber = theMobileNumber.standardFormat;
      const candid = await Candidate.findOne({
        mobileNumber: stdMobileNumber,
        active: true
      });
      if (candid) return true;
      return false;
    } catch (error) {
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[isCandidate.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside isCandidate.user.service"
        );
      }
    }
  };

  const addCandidate = async (data = {}) => {
    const { mobileNumber = null } = data;
    if (!mobileNumber) throw new ValidationError("mobileNumber is mandatory");
    try {
      const theMobileNumber = tools.parseMobileNumber(mobileNumber);
      if (!theMobileNumber) throw new ValidationError("mobileNumber is wrong");
      const stdMobileNumber = theMobileNumber.standardFormat;
      const options = { upsert: true, new: true, setDefaultsOnInsert: true };
      const query = { mobileNumber: stdMobileNumber };
      const theCandid = await Candidate.findOneAndUpdate(
        query,
        { active: true },
        options
      );

      return theCandid;
    } catch (error) {
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[addCandidate.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside addCandidate.user.service"
        );
      }
    }
  };

  const editCandidate = async (query = {}, newData = {}) => {
    const { mobileNumber = null } = query;
    if (!mobileNumber) throw new ValidationError("mobileNumber is mandatory");
    try {
      const theMobileNumber = tools.parseMobileNumber(mobileNumber);
      if (!theMobileNumber) throw new ValidationError("mobileNumber is wrong");
      const stdMobileNumber = theMobileNumber.standardFormat;
      const query = { mobileNumber: stdMobileNumber };
      const theCandid = await Candidate.findOneAndUpdate(query, newData, {
        new: true
      });

      return theCandid;
    } catch (error) {
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[editCandidate.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside addCandidate.user.service"
        );
      }
    }
  };

  const deleteManyCandidates = async query => {
    try {
      const user = await Candidate.deleteMany(query);
      return user;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "Invalid query parameters.");
      else {
        console.error("[deleteManyCandidates.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message ||
            "error happened inside deleteManyCandidates.user.service"
        );
      }
    }
  };

  /*
        This method handles the pre-processing of userData fields.
        It converts the userData.mobileNumber to the standard format.
        Other pre-processing may be added in future.
    */
  const normalizeUserData = (userData = {}) => {
    try {
      if (userData.mobileNumber) {
        const mobileNumber = tools.parseMobileNumber(userData.mobileNumber);
        userData.mobileNumber = mobileNumber.standardFormat;
      }
    } catch (error) {
      throw new ValidationError("invalid input for user data");
    }
  };

  /*
    This method updates the user by the data provided in newData.
    If no user is found, a new one is created and added to the database.

  */
  const UpsertUser = async (query, newData = {}, populate) => {
    try {
      const options = { upsert: true, new: true, setDefaultsOnInsert: true };
      const user = await User.findOneAndUpdate(
        query,
        newData,
        options
      ).populate(populate);
      return user;
    } catch (error) {
      const { status, error: userError } = errorService.buildErrorResponse({
        error,
        conditionalMap: {
          DatabaseError: { code: 105, custom: false, status: 500 }
        }
      });
      res.status(status).json({ error: userError });
    }
  };

  // This method updates the given user with the given data
  const updateUser = async (userId, newData) => {
    try {
      normalizeUserData(newData);
      const newUser = await User.findByIdAndUpdate(userId, newData, {
        new: true
      });
      return newUser;
    } catch (error) {
      if (error.type == params.errorTypes.ValidationError) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[updateUser.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside updateUser.user.service"
        );
      }
    }
  };

  const assignPrimaryLabToUser = async ({ userId, labId }) => {
    try {
      const newUser = await User.findByIdAndUpdate(
        userId,
        { primaryLab: labId },
        { new: true }
      );
      return newUser;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[assignPrimaryLabToUser.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message ||
            "error happened inside assignPrimaryLabToUser.user.service"
        );
      }
    }
  };

  /*
        This method adds the given labId to the list of labs of the user.

    */
  const addLabToUser = async ({ userId, labId }) => {
    try {
      const newUser = await User.findByIdAndUpdate(
        userId,
        { $addToSet: { labs: labId } },
        { new: true }
      );
      if (!newUser.primaryLab) return assignPrimaryLabToUser({ userId, labId });
      else return newUser;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[addLabToUser.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside addLabToUser.user.service"
        );
      }
    }
  };

  /*
        This method removes the lab from list of labs of user.
    */
  const removeLabFromUser = async ({ userId, labId }) => {
    try {
      const theUser = await findById(userId);
      if (!theUser)
        throw new ValidationError(`No user found with id ${userId}`);
      theUser.labs.pull(labId);
      if ((theUser.primaryLab || {}).toString() == labId)
        theUser.primaryLab = undefined;
      await theUser.save();
      return theUser;
    } catch (error) {
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "invalid input");
      else {
        console.error("[removeLabFromUser.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message ||
            "error happened inside removeLabFromUser.user.service"
        );
      }
    }
  };

  const deleteById = async userId => {
    try {
      const user = await User.findByIdAndRemove(userId);
      return user;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "Invalid user id.");
      else {
        console.error("[deleteById.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside deleteById.user.service"
        );
      }
    }
  };

  // This method deletes all users that satisfy the query from database
  const deleteMany = async query => {
    try {
      const user = await User.deleteMany(query);
      return user;
    } catch (error) {
      if (["ValidationError", "CastError"].includes(error.name))
        throw new ValidationError(error.message || "Invalid query parameters.");
      else {
        console.error("[deleteMany.user.service:] error happened:");
        console.error(error);
        throw new DatabaseError(
          error.message || "error happened inside deleteMany.user.service"
        );
      }
    }
  };

  const isAppOwner = mobileNumber => mobileNumber === appOwnerMobileNumber;

  const shouldReceiveNewUser = async ({ mobileNumber }) => {
    // After initial LIMITED phase, this method should always resolve with true

    /*
    The logic in the LIMITED phase:
    ONLY serve mobile numbers that:
    1. are already in our users' list.
    2. are in the candidates whitelist.
    3. are owners of our app! :)
    */

    try {
      const user = await findOne({ mobileNumber });
      if (user) return true;
      const isCandid = await isCandidate({ mobileNumber });
      if (isCandid) return isCandid;
      return isAppOwner(mobileNumber);
    } catch (error) {
      if (Object.keys(params.errorTypes).includes(error.type)) throw error;
      else {
        console.error("[shouldReceiveNewUser.user.service:] error happened:");
        console.error(error);
        throw new UnknownError(
          error.message ||
            "error happened inside shouldReceiveNewUser.user.service"
        );
      }
    }
  };

  return {
    getOrCreateUser,
    find: findUsers,
    findOne,
    findCandidates,
    addCandidate,
    editCandidate,
    isCandidate,
    update: updateUser,
    UpsertUser,
    findById,
    deleteById,
    normalizeUserData,
    addLabToUser,
    removeLabFromUser,
    assignPrimaryLabToUser,
    deleteMany,
    deleteManyCandidates,
    shouldReceiveNewUser,
    isAppOwner
  };
};
