const promisify = require("util").promisify;
const { ServiceError, PermissionError } = require("../utilities/my-errors");

module.exports = ({ jwt, jwtSecret, params }) => {
  const jwtSign = promisify(jwt.sign);
  const jwtVerify = promisify(jwt.verify);

  /*
        This method creates a jwt for the user. 
        It uses only the _id for the user in token payload.
    */
  const generateFor = async user => {
    try {
      const payload = { user: { _id: user._id } };
      const options = {
        expiresIn: params.expiresIn,
        issuer: params.issuer
      };
      const token = await jwtSign(payload, jwtSecret, options);
      return token;
    } catch (error) {
      console.error(
        "[generateFor.token.service: ]. Some problem happened when trying to create jwt token.:"
      );
      console.error(error);
      throw new ServiceError(
        error.message || "Something went wrong in generateFor.token.service"
      );
    }
  };

  /*
        This method verifies a jwt token.
        If the token signature is valid and expiration, audience, and issuer 
        are all valid, it resolves with the payload.Otherwise, it will reject with 
        an error.
    */
  const verify = async token => {
    const options = {
      issuer: params.issuer,
      clockTolerance: params.clockTolerance
    };
    try {
      const payload = await jwtVerify(token, jwtSecret, options);
      return payload;
    } catch (error) {
      throw new PermissionError(error.message || "token verification failed");
    }
  };

  /*
        Decodes the given jwt token and returns the payload.
    */
  const decode = token => jwt.decode(token);

  return { generateFor, verify, decode };
};
