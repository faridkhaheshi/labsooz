const request = require("request");
const { ServiceError } = require("../utilities/my-errors");

module.exports = smsConfig => {
  const sendSMS = options => {
    const form = {
      message: options.message,
      receptor: options.recipient
    };

    if (options.lineNumber) form.linenumber = options.lineNumber; // This parameter specifies the sender number (string)
    if (options.sendData) form.senddata = options.sendData; // This parameter specifies the exact time of sending the sms in Unixtime (string)
    if (options.checkId) form.checkid = options.checkId; // This parameter specifies a unique id for this request to enable checking status later.

    const reqOptions = {
      method: "POST",
      url: smsConfig.sendUrl,
      headers: { apikey: smsConfig.apiKey },
      form
    };

    return new Promise((resolve, reject) => {
      request(reqOptions, (error, response, body) => {
        if (error)
          reject(
            new ServiceError(
              `There is some problem with SMS service: ${error.message}`
            )
          );
        else if (response.statusCode == 200) resolve(body);
        else
          reject(
            new ServiceError(
              `There is some problem with SMS service provider. Here is the response received: ${body}`
            )
          );
      });
    });
  };

  return { send: sendSMS };
};
